# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2016-2018 Kyle Robbertze <kyle@aims.ac.za>
# This file is distributed under the same license as the Shortcuts package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-12-01 11:26+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../src/extension.js:66
msgid "The super key is the Windows key on most keyboards"
msgstr ""

#: ../src/extension.js:83
#, javascript-format
msgid "Shortcuts file not found: '%s'"
msgstr ""

#: ../src/extension.js:90
#, javascript-format
msgid "Unable to read file: '%s'"
msgstr ""

#: ../src/prefs.js:32
msgid "Custom Shortcuts File"
msgstr ""

#: ../src/prefs.js:37
msgid "Select shortcut file"
msgstr ""

#: ../src/prefs.js:49
msgid "Show tray icon"
msgstr ""

#: ../src/schemas/org.gnome.shell.extensions.shortcuts.gschema.xml:6
msgid "Use an alternative file for shortcut descriptions"
msgstr ""

#: ../src/schemas/org.gnome.shell.extensions.shortcuts.gschema.xml:7
msgid ""
"If true the shortcut file specified as shortcuts-file will be used for "
"custom shortcuts."
msgstr ""

#: ../src/schemas/org.gnome.shell.extensions.shortcuts.gschema.xml:11
msgid "Alternative file for shortcut descriptions"
msgstr ""

#: ../src/schemas/org.gnome.shell.extensions.shortcuts.gschema.xml:12
msgid ""
"If not empty it points to the shortcut file that will be read for the "
"shortcut descriptions."
msgstr ""

#: ../src/schemas/org.gnome.shell.extensions.shortcuts.gschema.xml:16
msgid "Toggles the icon on the top panel"
msgstr ""

#: ../src/schemas/org.gnome.shell.extensions.shortcuts.gschema.xml:17
msgid ""
"If true an icon to activate the extension will be visible on the top pannel."
msgstr ""
